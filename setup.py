from setuptools import setup, find_packages

setup(
    name='redshift-helper',
    version='0.12',
    packages=find_packages(),
    py_modules=['helper','db_utils'],
    install_requires=[
        'Click',
        'sqlalchemy-redshift',
        'psycopg2-binary',
        'python-dotenv'
    ],
    entry_points={
        'console_scripts': [
            "batch=helper:cli"
        ]
    },
)
