from sqlalchemy.engine import reflection, create_engine
from sqlalchemy.orm.session import sessionmaker
from contextlib import contextmanager
import logging

_session_ = None
_engine_ = None


def db_init(db_url, db_echo=False):
    global _session_, _engine_
    _engine_ = create_engine(db_url, echo=db_echo)
    _session_ = sessionmaker(_engine_)
    logging.info("connecting to: %s", db_url)


@contextmanager
def session():
    """Provide a transactional scope around a series of operations."""
    global _session_
    assert _session_
    session = _session_()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()
