# redshift helper

This tool enables you to execute a file containing a query and
have the output stored to another file as csv.

To install:
```
python3.6 -m venv venv
. venv/bin/activate
pip install git+https://bitbucket.org/blueshed/redshift-helper.git#egg=redshift-helper
```

Create a file called ".env" and put the connection url to the database
in it. For example you can run this on the command line:
```
echo db_url="postgresql+psycopg2://postgres@localhost:5439/wren_db" > .env
```
It will create a file and store the above sample connection url in it. You have to use the connection url provided by your admin and it should start
with "redshift+psycopg2://". This string is used by SQLAlchemy to connect to
the database.

Now you can create a query file. For example, a file named "test.sql". Type the following into a terminal:
```
echo "SELECT * FROM alembic_version" > test.sql
```

Then type into the terminal:
```
batch test.sql out.csv
```

You should see:
```
connected to postgresql+psycopg2://postgres@localhost:5439/wren_db
running query 'test.sql' -> 'out.csv'
done! 1 row(s).
```

And the contents of out.csv:
```
version_num
be2d7a1a5e43
```
