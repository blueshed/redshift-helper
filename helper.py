from sqlalchemy_redshift import dialect
from dotenv import load_dotenv
import db_utils
import csv
import click
import os


def execute_query(query):
    '''
        run a query and yield rows
    '''
    with db_utils.session() as session:
        yield from session.execute(query)


@click.command()
@click.argument('query', type=click.File('r'))
@click.argument('output', type=click.File('w'))
@click.option('-e','--env', type=str, default=".env", help="name of environment file containing db_url")
def cli(query, output, env):
    if not os.path.isfile(env):
        raise Exception(f"environment file missing! {env}")

    load_dotenv(env)
    db_url = os.getenv("db_url")
    db_utils.db_init(db_url)
    print(f"connected to {db_url}")

    writer = None
    print(f"running query {query.name !r} -> {output.name !r}")
    for i,row in enumerate(execute_query(query.read())):
        item = dict(row)
        if writer is None:
            writer = csv.DictWriter(output, fieldnames=row.keys())
            writer.writeheader()
        writer.writerow(item)
        if i != 0 and i % 1000 == 0:
            print(f"output {i} rows")
    print(f"done! {i+1} row(s).")
